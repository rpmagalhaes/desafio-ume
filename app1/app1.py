from flask import Flask, jsonify, redirect
from flask_restful import Resource, Api
from datetime import datetime
import requests

app = Flask(__name__)
api = Api(app)

class hello(Resource):
    def get(self):
        date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        try:
            response = jsonify(
                objetivo='Desafio Tecnico da UMe - Hello',
                criado_por='Rafael Magalhaes',
                data=date
            )
            response.status_code = 200
        except Exception as e:
            print(e)
            response = jsonify(message='Failed')
            response.status_code = 400
        finally:
            return(response)

class header(Resource):
    def get(self):
        date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        try:
            response = jsonify(
                objetivo='Desafio Tecnico da UMe - Header',
                criado_por='Rafael Magalhaes',
                data=date
            )
            response.status_code = 200
        except Exception as e:
            print(e)
            response = jsonify(message='Failed')
            response.status_code = 400
        finally:
            return(response)

class ping(Resource):
    def get(self):
        try:
            # ping = jsonify(request = 'ping')
            response = requests.get('http://app2:9090/api/v1/pong')
            # result = response.json()
            response.status_code = 200
        except Exception as e:
            print(e)
            response = jsonify(message='Failed')
            response.status_code = 400
        finally:
            return {'message': response.json()['message']}       
    
api.add_resource(hello, '/hello', endpoint='hello')
api.add_resource(header, '/header', endpoint='header')
api.add_resource(ping, '/api/v1/ping')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from datetime import datetime

app = Flask(__name__)
api = Api(app)

class pong(Resource):
    def get(self):
        date = datetime.today().strftime('%Y-%m-%d-%H:%M:%S')
        try:
            # json = request.get_json(force=True)
            # keys = list(json.keys())
            # if 'ping' in keys:
            #     response = jsonify(message='pong')
            # else:
            #     response = jsonify(message='error')
            response = jsonify(
                message='pong from app2'
            )
            response.status_code = 200
        except Exception as e:
            print(e)
            response = jsonify(message='Failed in pong')
            response.status_code = 400
        finally:
            return(response)
        
api.add_resource(pong, '/api/v1/pong')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=9090)
import multiprocessing
import unittest
import requests
from time import sleep
from app1.app1 import app as app1_instance
from app2.app2 import app as app2_instance

def start_app1():
    app1_instance.run(port=8080)


def start_app2():
    app2_instance.run(port=9090)


class TestIntegration(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app1_process = multiprocessing.Process(target=start_app1)
        cls.app2_process = multiprocessing.Process(target=start_app2)
        cls.app1_process.start()
        cls.app2_process.start()
        sleep(2)
    
    @classmethod
    def tearDownClass(cls):
        cls.app1_process.terminate()
        cls.app2_process.terminate()
    
    def test_integration(self):
        response = requests.get('http://localhost:8080/api/v1/ping')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'message': 'pong from app2'})

if __name__ == '__main__':
    unittest.main()